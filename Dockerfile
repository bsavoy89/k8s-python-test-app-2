FROM fc8dockq03.gfoundries.com:5000/centos:latest
WORKDIR /usr/src/app
COPY ./yum.conf /etc/yum.conf
ENV HTTP_PROXY=uswwwp1.gfoundries.com:74
ENV HTTPS_PROXY=uswwwp1.gfoundries.com:74

RUN yum update -y
RUN yum install net-tools tcpdump traceroute python -y
RUN curl "https://bootstrap.pypa.io/get-pip.py" -o "get-pip.py"
RUN python get-pip.py
RUN pip install --upgrade setuptools
RUN pip --no-cache-dir install web.py flask
ENV HTTP_PROXY=""
ENV HTTPS_PROXY=""
copy ./pytest.py ./

CMD ["python", "./pytest.py"]
EXPOSE 8080
