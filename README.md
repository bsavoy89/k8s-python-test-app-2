#### Bradley Savoy
Simple python application to test a couple of facets of a k8s system

1 - Container Network - Test the ability of an application to communicate with another service in the cluster (a database)

2 - Ingress - Demonstrate the ability o an application to be accessible from outside the cluster 

3 - Horizontal Scaling - Demonstrate the ability of an application to scale transparently 

4 - Rolling update - Demonstrate the ability of an application to update automatically
