#!flask/bin/python
from flask import Flask
import time
import socket
import json
import urllib

app = Flask(__name__)
@app.route('/listclusterpods')
def getPods():
    #Iterate through all namespaces and collect pods from each
    outString = 'Current Pods in Cluster' 
    allNs = urllib.urlopen("http://fc8k8smqv.gfoundries.com:8080/api/v1/namespaces?limit=500")
    nsJson = json.loads(allNs.read())
    for nsItem in nsJson['items']:
        res = urllib.urlopen("http://fc8k8smqv.gfoundries.com:8080/api/v1/namespaces/" + nsItem['metadata']['name'] + "/pods?limit=500")
        output_json = json.loads(res.read())
        for item in output_json['items']:
            outString += '<br />' + item['metadata']['name']
    return outString

@app.route('/')
def index():
    host = socket.gethostname()
    return 'Hello, human' + ' from ' + socket.gethostbyname(host) + ' !'

@app.route('/healthcheck')
def health():
#Play with this sleep to demonstrate health checking
    time.sleep(5) 
    isUp = True   
    return "ok"
@app.route('/custommessage')
def customMessage():
    return "I like eggs!"
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, debug=True)
